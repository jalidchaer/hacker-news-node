<p align="center">
  <a href="http://nestjs.com/" target="blank"><img src="https://nestjs.com/img/logo-small.svg" width="200" alt="Nest Logo" /></a>
</p>

# Hacker News

1. Clone the repository
2. Run
```
yar ninstall
```

3. Have Nest CLI installed
```
npm i -g @nestjs/cli
```

4. Raise the database
```
docker-compose up -d
```

5. Run the app on dev:
```
yarn start:dev
```

8. Rebuild the database with the seed
```
http://localhost:3000/seed
```

## Stack 
*MongoDB
*Nest