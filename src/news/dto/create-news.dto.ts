import { IsArray, IsInt, IsString } from 'class-validator';
export class CreateNewsDto {
  @IsString()
  created_at?: string;
  @IsString()
  title: string;
  @IsString()
  url: string;
  @IsString()
  author: string;
  @IsInt()
  points: number;
  @IsString()
  story_text: string;
  @IsString()
  comment_text: string;
  @IsInt()
  num_comments: number;
  @IsInt()
  story_id: number;
  @IsString()
  story_title: string;
  @IsString()
  story_url: string;
  @IsInt()
  parent_id: number;
  @IsInt()
  created_at_i: number;
  @IsArray()
  tags: string[];
  @IsString()
  objectID: string;
}
