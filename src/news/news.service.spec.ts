import { Test, TestingModule } from '@nestjs/testing';
import { getModelToken } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { News } from 'src/news/entities/news.entity';
import { NewsService } from './news.service';
import { AxiosAdapter } from 'src/common/adapters/axios.adapter';
import { AxiosResponse } from 'axios';
jest.mock('axios');

const mockCustomHttpService = {
  get: jest.fn(),
};

describe('NewsService', () => {
  let service: NewsService;
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        NewsService,
        { provide: getModelToken(News.name), useValue: Model },
        { provide: AxiosAdapter, useValue: mockCustomHttpService },
      ],
      exports: [NewsService],
    }).compile();

    service = module.get<NewsService>(NewsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('getDataNews', async () => {
    const data = {
      data: {
        hits: [
          {
            created_at: '2022-10-11T13:48:14.000Z',
            title: null,
            url: null,
            author: 'Fymer',
            points: null,
            story_text: null,
            comment_text:
              '<p><pre><code>  Location: Lisboa, Portugal\n  Remote: Yes, remote only.\n  Willing to relocate: No.\n  Technologies: ReactJS, NextJS, Redux, Typescript, Tailwind. Can do a bit of React-Native and even a bit of NodeJS if needed.\n  Résumé&#x2F;CV: https:&#x2F;&#x2F;www.linkedin.com&#x2F;in&#x2F;jo%C3%A3oplgomes&#x2F;\n  Email: joaogomesapps[at]gmail{dot}com\n</code></pre>\nI&#x27;m a Frontend Developer that works mainly on ReactJS with Typescript and Redux. Can also work on React-Native for mobile apps and can also help in the backend in NodeJS.\nCurrently searching for interesting full-time(remote) opportunities where I would be a good fit with my experience and provide the most value.',
            num_comments: null,
            story_id: 33068418,
            story_title: 'Ask HN: Who wants to be hired? (October 2022)',
            story_url: null,
            parent_id: 33068418,
            created_at_i: 1665496094,
            _tags: ['comment', 'author_Fymer', 'story_33068418'],
            objectID: '33163320',
          },
        ],
      },
    };
    const response: AxiosResponse = {
      data,
      headers: {},
      config: {},
      status: 200,
      statusText: 'OK',
    };
    jest.spyOn(service, 'getDataNews').mockImplementation(() => response.data);

    expect(await service.getDataNews()).toBe(response.data);
  });
});
