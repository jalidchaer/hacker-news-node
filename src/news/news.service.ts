import {
  BadRequestException,
  Injectable,
  InternalServerErrorException,
  NotFoundException,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { News } from 'src/news/entities/news.entity';
import { PaginationDto } from 'src/common/dto/pagination.dto';
import { AxiosAdapter } from 'src/common/adapters/axios.adapter';
import { NewsResponse } from 'src/news/interfaces/news-response.interface';

@Injectable()
export class NewsService {
  constructor(
    @InjectModel(News.name)
    private readonly newsModel: Model<News>,
    private readonly http: AxiosAdapter,
  ) {}

  async insertMany(data) {
    await this.newsModel.insertMany(data);
  }
  async deleteMany(data) {
    await this.newsModel.deleteMany(data);
  }

  async getDataNews() {
    const data = await this.http.get<NewsResponse>(
      'https://hn.algolia.com/api/v1/search_by_date?query=nodejs',
    );
    return data;
  }

  async getAllNews() {
    return await this.newsModel.find().select('-__v');
  }

  async findAll(paginationDto: PaginationDto) {
    const { limit = 5, offset = 0 } = paginationDto;
    return await this.newsModel
      .find()
      .where({ status: true })
      .limit(limit)
      .skip(offset)
      .sort({
        no: 1,
      })
      .select('-__v');
  }

  async find(term: string) {
    let news: News[] = [];
    let options = {};
    if (term) {
      options = {
        $or: [
          { author: new RegExp(term.toString(), 'i') },
          { title: new RegExp(term.toString(), 'i') },
          { story_title: new RegExp(term.toString(), 'i') },
          { _tags: new RegExp(term.toString(), 'i') },
        ],
      };
    }
    news = await this.newsModel.find(options).where({ status: true });
    return news;
  }

  async remove(id: string) {
    try {
      const news = await this.newsModel.findByIdAndUpdate(
        id,
        { status: false },
        { new: true },
      );
      return news;
    } catch (error) {
      this.handleExceptions(error);
    }
  }

  private handleExceptions(error: any) {
    if (error.code === 11000) {
      throw new BadRequestException(
        `News exists in db ${JSON.stringify(error.keyValue)}`,
      );
    }
    console.log(error);
    throw new InternalServerErrorException(
      `Can't create News - Check server logs`,
    );
  }
}
