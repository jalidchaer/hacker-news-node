import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema()
export class News extends Document {
  @Prop({ type: String })
  created_at: string;

  @Prop({ type: String })
  title: string;

  @Prop({ type: String })
  url: string;

  @Prop({ type: String })
  author: string;

  @Prop({ type: Number })
  points: number;

  @Prop({ type: String })
  story_text: string;

  @Prop({ type: String })
  comment_text: string;

  @Prop({ type: Number })
  num_comments: number;

  @Prop({ type: Number })
  story_id: number;

  @Prop({ type: String })
  story_title: string;

  @Prop({ type: String })
  story_url: string;

  @Prop({ type: Number })
  parent_id: number;

  @Prop({ type: Number })
  created_at_i: number;

  @Prop({ type: [String] })
  _tags: string[];

  @Prop({ type: String })
  objectID: string;

  @Prop({ type: Boolean, default: true })
  status: boolean;
}

export const NewsSchema = SchemaFactory.createForClass(News);
