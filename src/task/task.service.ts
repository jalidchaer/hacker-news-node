import { Injectable } from '@nestjs/common';
import { News } from 'src/news/entities/news.entity';
import { Cron, CronExpression } from '@nestjs/schedule';
import { NewsService } from 'src/news/news.service';

@Injectable()
export class TaskService {
  constructor(private readonly newsService: NewsService) {}
  @Cron(CronExpression.EVERY_HOUR)
  async getNews() {
    let allNews: News[] = [];
    // get the news
    allNews = await this.newsService.getAllNews();

    const data = await this.newsService.getDataNews();
    // checking if the news has been deleted or exist no insert again
    const newsToInsert = [];
    data.hits.forEach((news) => {
      let findNews = [];
      if (allNews) {
        findNews = allNews.filter(
          (result) => result.objectID === news.objectID,
        );
      }

      if (!findNews.length) {
        newsToInsert.push(news);
      }
    });
    if (newsToInsert.length) {
      await this.newsService.insertMany(newsToInsert);
    }

    return 'News updated';
  }
}
