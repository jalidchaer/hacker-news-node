import { Test, TestingModule } from '@nestjs/testing';
import { getModelToken } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { News } from 'src/news/entities/news.entity';
import { CommonModule } from 'src/common/common.module';
import { NewsModule } from 'src/news/news.module';
import { TaskController } from './task.controller';
import { TaskService } from './task.service';
import { AxiosAdapter } from 'src/common/adapters/axios.adapter';

describe('TaskController', () => {
  let controller: TaskController;
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [TaskController],
      providers: [TaskService],
      imports: [NewsModule, CommonModule],
    }).compile();

    controller = module.get<TaskController>(TaskController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
