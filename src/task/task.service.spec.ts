import { Test, TestingModule } from '@nestjs/testing';
import { TaskService } from './task.service';
import { NewsService } from 'src/news/news.service';
import { NewsModule } from 'src/news/news.module';

describe('TaskService', () => {
  let service: TaskService;
  let newsService: NewsService;
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [TaskService],
      imports: [NewsModule],
    }).compile();

    service = module.get<TaskService>(TaskService);
    newsService = module.get<NewsService>(NewsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
