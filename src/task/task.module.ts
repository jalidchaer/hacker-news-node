import { Module } from '@nestjs/common';
import { TaskService } from './task.service';
import { TaskController } from './task.controller';
import { CommonModule } from 'src/common/common.module';
import { NewsModule } from 'src/news/news.module';

@Module({
  controllers: [TaskController],
  providers: [TaskService],
  imports: [NewsModule, CommonModule],
})
export class TaskModule {}
