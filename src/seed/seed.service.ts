import { Injectable } from '@nestjs/common';
import { NewsService } from 'src/news/news.service';

@Injectable()
export class SeedService {
  constructor(private readonly newsService: NewsService) {}

  async executeSeed() {
    await this.newsService.deleteMany({});
    const data = await this.newsService.getDataNews();

    const newsToInsert = [];
    data.hits.forEach((news) => {
      newsToInsert.push(news);
    });

    await this.newsService.insertMany(newsToInsert);
    return 'Seed executed';
  }
}
