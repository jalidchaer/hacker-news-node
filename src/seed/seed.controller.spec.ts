import { getModelToken } from '@nestjs/mongoose';
import { Test, TestingModule } from '@nestjs/testing';
import { Model } from 'mongoose';
import { News } from 'src/news/entities/news.entity';
import { CommonModule } from 'src/common/common.module';
import { NewsService } from 'src/news/news.service';
import { SeedController } from './seed.controller';
import { SeedService } from './seed.service';
import { AxiosAdapter } from 'src/common/adapters/axios.adapter';

describe('SeedController', () => {
  let controller: SeedController;
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [SeedController],
      providers: [SeedService, { provide: NewsService, useValue: jest.fn() }],
    }).compile();

    controller = module.get<SeedController>(SeedController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
