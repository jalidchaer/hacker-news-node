import { getModelToken } from '@nestjs/mongoose';
import { Test, TestingModule } from '@nestjs/testing';
import { SeedService } from './seed.service';
import { Model } from 'mongoose';
import { News } from 'src/news/entities/news.entity';
import { AxiosAdapter } from 'src/common/adapters/axios.adapter';
import { NewsService } from 'src/news/news.service';

describe('SeedService', () => {
  let service: SeedService;
  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [SeedService, { provide: NewsService, useValue: jest.fn() }],
    }).compile();

    service = module.get<SeedService>(SeedService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
